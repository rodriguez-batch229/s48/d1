// Dummy database

let posts = [],
    count = 1;

// Add post data
document.querySelector("#form-add-post").addEventListener("submit", (event) => {
    // this will prevent our webpage from reloading after clicking submit
    event.preventDefault();
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value,
    });
    count++;

    showPosts(posts);
    alert("Successfully Added!");
});

// Show posts
const showPosts = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}" >
                ${post.title}
            </h3>
            <p id="post-body-${post.id}">
                ${post.body}
            </p>

            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        
        </div>

        `;
    });

    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML,
        body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};

// delete Post

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });

    document.querySelector(`#post-${id}`).remove();
};

// update post

document
    .querySelector("#form-edit-post")
    .addEventListener("submit", (event) => {
        event.preventDefault();
        for (let i = 0; i < posts.length; i++) {
            // The value post[i].id is a number while document.querySelector("#txt-edit-id").value is a string
            // Therefore it is necessary to convert the number to a string

            if (
                posts[i].id.toString() ===
                document.querySelector("#txt-edit-id").value
            ) {
                posts[i].title =
                    document.querySelector("#txt-edit-title").value;
                posts[i].body = document.querySelector("#txt-edit-body").value;

                showPosts(posts);
                alert("Successfully updated");

                break;
            }
        }
    });
